from datetime import timedelta

from dateutil.tz import tzlocal, UTC

from .core import *
from .patterns import *

def exifTs2PythonTs(exifTs: str) -> datetime:
    return datetime.strptime(exifTs, "%Y:%m:%d %H:%M:%S").astimezone(tzlocal())

def mp4Ts2PythonTs(mp4ts: str) -> datetime:
    return datetime.strptime(mp4ts, "%Y:%m:%d %H:%M:%S").replace(tzinfo=UTC).astimezone(tzlocal())

def pythonTs2exifTs(dt: datetime) -> str:
    return dt.strftime('%Y:%m:%d %H:%M:%S')

def pythonTs2mp4Ts(dt: datetime) -> str:
    return dt.astimezone(UTC).strftime('%Y:%m:%d %H:%M:%S')

def compute_JPG_timestamp(pict: MediaFile) -> None:
    """ Heuristics to figure out the timestamp, when the Exif header does not tell
    """
    # Try matching with any of the patterns coding a full timestamp
    m = pixelExportedPattern.search(pict.origBase)
    if m:
        # already set by retrieveMetadata:  pict.cameraModel = "Pixel 3a"
        # Pixel naming is based on UTC timestamp, not local timestamp
        pict.timestamp = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')),
                                  hour=int(m.group('hour')), minute=int(m.group('minute')), second=int(m.group('sec'))
                                  ).replace(tzinfo=UTC).astimezone(tzlocal())
        return

    m = linuxWhatsAppPattern.search(pict.origBase)
    if m:
        pict.cameraModel = WHATSAPP
        pict.cameraMaker = SOCIALNETWORK
    else:
        # May be the file name is cannonical, but the Exif header is missing ?
        m = cannonicalNamePattern.search(pict.origBase)
    if m:
        pict.timestamp = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')),
                              hour=int(m.group('hour')), minute=int(m.group('minute')), second=int(m.group('sec'))
                              ).astimezone(tzlocal())
        return
    # Try the date-only WhatsApp pattern
    m = androidWhatsAppPattern.search(pict.origBase)
    if m:
        pict.cameraModel = WHATSAPP
        pict.cameraMaker = SOCIALNETWORK
        pict.timestamp = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')),
                                  hour=12, minute=0, second=0
                                  ).astimezone(tzlocal())
        return
    # Try to find a timestamp anywhere in the file name
    m = datedNamePattern.search(pict.origName)
    if m:
        pict.timestamp = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')),
                                  hour=int(m.group('hour')), minute=int(m.group('minute')),
                                  second=int(m.group('sec'))
                                  ).astimezone(tzlocal())
        return
    # No clue, assign current datetime
    pict.timestamp = datetime.now().astimezone(tzlocal())

def compute_MP4_timestamp(pict: MediaFile) -> None:
    # Note: as of 2023-11, exiv2 fails to read XMP dates from a Pixel video
    # Try file name patterns
    m = linuxWhatsAppPattern.search(pict.origBase)
    if m:
        pict.cameraModel = WHATSAPP
        pict.cameraMaker = SOCIALNETWORK
    else:
        m = cannonicalNamePattern.search(pict.origBase)
    if m:
        pict.timestamp = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')),
                                  hour=int(m.group('hour')), minute=int(m.group('minute')),
                                  second=int(m.group('sec'))
                                  ).astimezone(tzlocal())
        return
    # Try the date-only WhatsApp pattern
    m = androidWhatsAppPattern.search(pict.origBase)
    if m:
        pict.cameraModel = WHATSAPP
        pict.cameraMaker = SOCIALNETWORK
        pict.timestamp = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')),
                                  hour=12, minute=0, second=0
                                  ).astimezone(tzlocal())
        return
    # Try to find a timestamp anywhere in the file name
    m = datedNamePattern.search(pict.origName)
    if m:
        pict.timestamp = datetime(int(m.group('year')), int(m.group('month')), int(m.group('day')),
                                  hour=int(m.group('hour')), minute=int(m.group('minute')),
                                  second=int(m.group('sec'))
                                  ).astimezone(tzlocal())
        return
    # No clue, assign current datetime
    pict.timestamp = datetime.now().astimezone(tzlocal())

def computeTimestamps(picts: [MediaFile]) -> None:
    """ Parse timestamp, in name or somewhere else """
    for pict in picts:
        if pixelExportedPattern.search(pict.origBase):
            # Pixel exported files have an embedded timestamp, but it is wrong (time of exportation),
            # so we decide here to ignore it, and flag for an update at the end.
            pict.timestamp = None
            pict.metadataToBeUpdated = True
        if pict.timestamp is not None:
            continue
        pict.computedTimestamp = True
        if pict.extension == JPG:
            compute_JPG_timestamp(pict)
        elif pict.extension == MP4:
            compute_MP4_timestamp(pict)
        else:
            print(f"Unsupported file extension: {pict.extension}")
    for pict in picts:
        pict.origTimestamp = pict.timestamp

def interpolateDate(lowDate:datetime, highDate:datetime, moveAmount: int, allowJumpOver: bool) -> datetime:
    delta = highDate - lowDate
    if allowJumpOver and delta <= timedelta(seconds=1):
        # Enable jumping over the closest pict: move by at least 1 second
        if moveAmount > 0:
            return lowDate.replace(microsecond=0) + timedelta(seconds=1)
        else:
            return lowDate.replace(microsecond=0) - timedelta(seconds=1)

    middleDate:datetime = lowDate + delta/2
    if delta > timedelta(hours=6):
        # round to closest hour
        return middleDate.replace(second=0, microsecond=0, minute=0)+timedelta(hours=middleDate.minute // 30)
    elif delta > timedelta(hours=1):
        # round to closest 10mn
        t = middleDate+timedelta(minutes=5)
        return t.replace(minute=t.minute//10*10, second=0, microsecond=0)
    elif delta > timedelta(minutes=10):
        # round to the closest minute
        return middleDate.replace(second=0, microsecond=0)+timedelta(minutes=middleDate.second // 30)
    elif delta > timedelta(minutes=1):
        # round to closest 10s
        t = middleDate+timedelta(seconds=5)
        return t.replace(second=t.second//10*10, microsecond=0)
    else:
        return middleDate.replace(microsecond=0)

