from __future__ import annotations  # Not enough for PyCharm, so used quoted names below
from typing import Callable

from PyQt5.QtCore import Qt, QModelIndex, QSize
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QAction, QMenu, QApplication, QDialog, QVBoxLayout, QScrollArea, QLabel, QMainWindow, \
    QToolBar, QStyle, QMenuBar

from .core import MediaFile
from .process import computeChanges, generateScript, applyChanges
from .view import RenameTableView


class ChangesWindow(QDialog):
    def __init__(self, window: QMainWindow, text: str):
        super().__init__(window)
        self.setWindowTitle("Modifications")
        self.layout = QVBoxLayout(self)
        self.scrollArea = QScrollArea(self)
        self.label = QLabel(self)
        self.label.setFont(QFont('Monospace', 10, QFont.Monospace))
        self.label.setTextInteractionFlags(Qt.TextSelectableByMouse)
        # self.message.setSizePolicy(QSizePolicy.Policy.Preferred, QSizePolicy.Policy.Preferred)  # default, actually
        self.label.setText(text)
        # self.message.adjustSize()  # Only valid when the widget is already drawn, need a timer ...
        self.scrollArea.setWidget(self.label)
        self.layout.addWidget(self.scrollArea)
        self.setLayout(self.layout)


class ActionsConfigurator:
    def __init__(self, app: QApplication, window: QMainWindow, view: RenameTableView, picts: [MediaFile]):
        self.window = window
        self.view = view  # contextMenuBuilder needs it
        self.picts = picts

        self.jumpUpAction = self.buildAction(
            'Jump Up',  Qt.ALT + Qt.Key_Up, "Jump picture one row up", "SP_ArrowUp",
            lambda: view.movePict(-view.numColumns)
        )
        self.jumpDownAction = self.buildAction(
            'Jump Down', Qt.ALT + Qt.Key_Down, "Jump picture one row down", "SP_ArrowDown",
            lambda: view.movePict(view.numColumns)
        )
        self.jumpLeftAction = self.buildAction(
            'Jump Left', Qt.ALT + Qt.Key_Left, "Jump picture over the next left", "SP_ArrowLeft",
            lambda: view.movePict(-1)
        )
        self.jumpRightAction = self.buildAction(
            'Jump Right', Qt.ALT + Qt.Key_Right, "Jump picture over the next right", "SP_ArrowRight",
            lambda: view.movePict(1)
        )
        self.closerLeftAction = self.buildAction(
            'Closer Left', Qt.CTRL+Qt.ALT+Qt.Key_Left, "Move picture closer to the next left", "SP_MediaSeekBackward",
            lambda: view.moveCloser(-1)
        )
        self.closerRightAction = self.buildAction(
            'Closer Right', Qt.CTRL+Qt.ALT+Qt.Key_Right, "Move picture closer to the next right", "SP_MediaSeekForward",
            lambda: view.moveCloser(1)
        )
        self.oneHourMoreAction = self.buildAction(
            'Add 1 hour', Qt.CTRL+Qt.ShiftModifier+Qt.ALT+Qt.Key_Right, "Add 1 hour to the timestamp", None,
            lambda: view.moveByHour(1)
        )
        self.oneHourLessAction = self.buildAction(
            'Substract 1 hour', Qt.CTRL+Qt.ShiftModifier+Qt.ALT+Qt.Key_Left, "Substract 1 hour to the timestamp", None,
            lambda: view.moveByHour(-1)
        )
        self.deleteAction = self.buildAction(
            'Delete', Qt.Key_Delete, "Delete the picture", "SP_TrashIcon",
            view.deletePict
        )
        self.applyAction = self.buildAction(
            'Save/Apply', Qt.CTRL + Qt.Key_S, "Apply changes", "SP_DialogSaveButton",
            lambda: [applyChanges(picts), view.model().layoutChanged.emit()]
        )
        self.showChangesAction = QAction('Show changes')
        self.showChangesAction.setStatusTip("Show the new names of files")
        self.showChangesAction.triggered.connect(lambda: ChangesWindow(window, computeChanges(self.picts)).exec())

        self.genScriptAction = QAction('Generate shimpla-script.sh')
        self.genScriptAction.setStatusTip("shimpla-script.sh contains commands to apply changes")
        self.genScriptAction.triggered.connect(lambda: generateScript(self.picts))

        self.exitAction = QAction('Exit')
        self.exitAction.setStatusTip("Exit")
        self.exitAction.triggered.connect(lambda: app.exit(0))

    def buildAction(self, name: str, shortcut, statusTip: str, iconName: str | None, func: Callable) -> QAction:
        action = QAction(name)
        action.setShortcut(shortcut)
        action.setStatusTip(statusTip)
        action.triggered.connect(func)
        if iconName is not None:
            action.setIcon(self.view.style().standardIcon(getattr(QStyle, iconName)))
        return action

    def configureViewActions(self):
        self.view.addAction(self.jumpUpAction)  # Could also be added to a Toolbar, with an Icon
        self.view.addAction(self.jumpDownAction)
        self.view.addAction(self.jumpLeftAction)
        self.view.addAction(self.jumpRightAction)
        self.view.addAction(self.closerLeftAction)
        self.view.addAction(self.closerRightAction)
        self.view.addAction(self.oneHourMoreAction)
        self.view.addAction(self.oneHourLessAction)
        self.view.addAction(self.deleteAction)
        self.view.addAction(self.applyAction)

    def configureToolBarActions(self, actionsToolBar: QToolBar, menuBar: QMenuBar):
        actionsToolBar.setIconSize(QSize(16, 16))
        actionsToolBar.addWidget(menuBar)
        actionsToolBar.addSeparator()
        actionsToolBar.addAction(self.jumpUpAction)
        actionsToolBar.addAction(self.jumpDownAction)
        actionsToolBar.addAction(self.jumpLeftAction)
        actionsToolBar.addAction(self.jumpRightAction)
        actionsToolBar.addAction(self.closerLeftAction)
        actionsToolBar.addAction(self.closerRightAction)
        actionsToolBar.addAction(self.oneHourMoreAction)
        actionsToolBar.addAction(self.oneHourLessAction)
        actionsToolBar.addAction(self.deleteAction)

    def configureFileMenuActions(self, fileMenu: QMenu):
        fileMenu.addAction(self.showChangesAction)
        fileMenu.addAction(self.genScriptAction)
        fileMenu.addAction(self.applyAction)
        fileMenu.addAction(self.exitAction)

    def configureContextMenu(self, event) -> None:
        model = self.view.model()
        menu: QMenu = QMenu()
        index: QModelIndex = self.view.indexAt(event.pos())
        cameraSelectAction: QAction = menu.addAction('',)
        cameraShiftAction: QAction = menu.addAction('')
        pict: MediaFile | None = None
        # TODO: provide a datetime selector, for arbitrary changes
        if index.isValid():
            preview = index.data()
            if preview is None:
                return
            pict = preview.mediafile
            cameraSelectAction.setText(f"Highlight all {pict.cameraModel}")
            if not pict.computedTimestamp:
                if model.shiftingCameraModel == pict.cameraModel:
                    cameraShiftAction.setText(f"Lock all {pict.cameraModel}")
                else:
                    cameraShiftAction.setText(f"Unlock all {pict.cameraModel}")
            menu.addSeparator()
            if pict.computedTimestamp or model.shiftingCameraModel == pict.cameraModel:
                menu.addAction(self.jumpLeftAction)
                menu.addAction(self.jumpRightAction)
                menu.addAction(self.jumpDownAction)
                menu.addAction(self.jumpUpAction)
                menu.addAction(self.closerLeftAction)
                menu.addAction(self.closerRightAction)
                menu.addAction(self.oneHourMoreAction)
                menu.addAction(self.oneHourLessAction)
            if pict.computedTimestamp:
                menu.addAction(self.deleteAction)
        else:
            cameraSelectAction.setText('No selection')
            cameraShiftAction.setText('No selection')
            cameraSelectAction.setEnabled(False)
            cameraShiftAction.setEnabled(False)
        res = menu.exec_(event.globalPos())
        if res == cameraSelectAction:
            self.view.selectByCameraModel(pict.cameraModel)
        elif res == cameraShiftAction:
            if model.shiftingCameraModel == pict.cameraModel:
                model.setShiftingCameraModel(model.NOSELECTION)
            else:
                model.setShiftingCameraModel(pict.cameraModel)
