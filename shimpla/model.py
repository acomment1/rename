import math
from collections import namedtuple
from datetime import timedelta
from typing import TypeAlias

from PyQt5.QtCore import QAbstractTableModel, Qt
from PyQt5.QtGui import QImage
from PyQt5.QtWidgets import QStyle

from .core import *
from .timestamps import interpolateDate
from .video import Thumbnailer

# In this Qt application, the model is a list of custom namedtuple classes.
preview = namedtuple("preview", "id mediafile previewdata")
Model: TypeAlias = list[preview]


class PreviewData:

    thumbNailer: Thumbnailer | None = None

    def __init__(self, pict: MediaFile):
        self.pict = pict
        self.thumbnail: QImage | None = None
        self.lastThumbWidth: int | None = None
        self.lastCellHeight: int | None = None

        self.largeThumb: QImage
        if pict.extension == 'jpg':
            self.largeThumb = QImage(pict.origName)
        else:
            self.largeThumb = self.thumbNailer.buildLargeVideoThumb(pict.origName)


class PreviewModel(QAbstractTableModel):
    NOSELECTION = "NoSelection"

    def __init__(self, view):
        super().__init__()
        self.tableView = view
        # .data holds our data for display, as a list of Preview objects.
        self.previews: Model = []
        self.shiftingCameraModel: str = self.NOSELECTION

    def data(self, index, role):
        try:
            data = self.previews[self.tableView.getScalarIndex(index)]
        except IndexError:
            # Incomplete last row.
            return

        if role == Qt.DisplayRole:
            return data   # Pass the data to our delegate to draw.

        if role == Qt.ToolTipRole:
            return data.mediafile.origName

        # if role == Qt.BackgroundRole and data.mediafile.computedTimestamp:
        #     return QColor('blue')

    def columnCount(self, index):
        # In our case, the model is not a database table, but a fully dynamic table
        # The number of columns is actually decided by the view, according to how many can fit in the window
        return self.tableView.numColumns

    def rowCount(self, index):
        n_items = len(self.previews)
        n_rows = math.ceil(n_items / self.tableView.numColumns)
        return n_rows

    def loadPicts(self, picts: [MediaFile], style: QStyle):
        PreviewData.thumbNailer = Thumbnailer(style)
        # Add a bunch of images.
        for n, pict in enumerate(picts):
            if pict.toBeDeleted:
                continue
            item = preview(n, pict, PreviewData(pict))
            self.previews.append(item)
        self.layoutChanged.emit()

    def movePict(self, idx: int, moveAmount: int) -> int:
        """ Move an image in the model.
            The sign of moveAmount gives the direction: forward or backward.
            The value of moveAmount gives the quantity: one cell, or one row.
            Target is in bounds, it has been checked by the caller
        """
        previews: Model = self.previews
        refPreview = previews[idx]
        pict: MediaFile = refPreview.mediafile
        cameraShiftMode: bool = self.shiftingCameraModel == pict.cameraModel

        if cameraShiftMode:
            # Checks: in cameraShiftMode, do not shift if all further picts are of the same cameraModel
            jumpOverOther: bool = False
            if moveAmount > 0:
                for nIdx in range(idx, len(previews)):
                    if previews[nIdx].mediafile.cameraModel != pict.cameraModel:
                        jumpOverOther = True
                        break
                if not jumpOverOther:
                    return len(previews) - 1  # Position at end of list
            else:
                for nIdx in range(0, idx):
                    if previews[nIdx].mediafile.cameraModel != pict.cameraModel:
                        jumpOverOther = True
                        break
                if not jumpOverOther:
                    return 0                 # Position at beginning of list

        # We move idx at the place of targetIdx
        targetIdx: int = idx + moveAmount

        if moveAmount > 0:
            if targetIdx == len(previews)-1:  # taking the last place
                newTimestamp = previews[-1].mediafile.timestamp + timedelta(seconds = 2)
            else:
                newTimestamp = interpolateDate(previews[targetIdx].mediafile.timestamp,
                                                previews[targetIdx+1].mediafile.timestamp,
                                                moveAmount, allowJumpOver=True)
        else:
            if targetIdx == 0:  # taking the first place
                newTimestamp = previews[0].mediafile.timestamp - timedelta(seconds = 2)
            else:
                newTimestamp = interpolateDate(previews[targetIdx-1].mediafile.timestamp,
                                                previews[targetIdx].mediafile.timestamp,
                                                moveAmount, allowJumpOver=True)
        # reorder data
        if cameraShiftMode:
            self.adjustAllSameModel(pict, newTimestamp)
        else:
            # Apply only to this pict
            pict.timestamp = newTimestamp
            previews.insert(targetIdx, previews.pop(idx))
        newSelectedIndex: int = previews.index(refPreview)
        # redraw
        self.layoutChanged.emit()
        return newSelectedIndex

    def adjustAllSameModel(self, pict: MediaFile, newTimestamp: datetime):
        timeShift: timedelta = newTimestamp - pict.timestamp
        # Apply same timedelta to all picts with same Exif Model (including the pict in argument)
        for prevw in self.previews:
            aPict = prevw.mediafile
            if aPict.cameraModel == pict.cameraModel:
                aPict.timestamp = aPict.timestamp + timeShift
        # Re-order the GUI model by timestamp
        # self.previews.sort(key=lambda prevw: (prevw.mediafile.timestamp, prevw.mediafile.origName))
        # Do NOT sort also by origName here, or the selected pict might jump over its neighbour during a moverCloser
        self.previews.sort(key=lambda prvw: prvw.mediafile.timestamp)

    def moveCloser(self, idx: int, direction: int):
        """ Adjust the date of idx to be closer to the mediafile next to it """
        previews: Model = self.previews
        pict = previews[idx].mediafile
        if direction > 0:  # Move to right
            if idx >= len(previews) - 1:
                return  # out of bounds, we are the last image
            lowDate = pict.timestamp
            highDate = previews[idx+1].mediafile.timestamp
        else:              # Move to left
            if idx < 1:
                return  # No image on our left
            lowDate = previews[idx-1].mediafile.timestamp
            highDate = pict.timestamp
        newTimestamp = interpolateDate(lowDate, highDate, direction, allowJumpOver=False)

        if self.shiftingCameraModel == pict.cameraModel:
            self.adjustAllSameModel(pict, newTimestamp)
        else:
            previews[idx].mediafile.timestamp = newTimestamp

        self.layoutChanged.emit()

    def moveByHour(self, idx: int, moveAmount: int) -> int:
        # Move by moveAmount hours (forward or backward)
        previews: Model = self.previews
        refPreview = previews[idx]
        pict = previews[idx].mediafile
        newTimestamp = pict.timestamp + timedelta(hours=moveAmount)
        if self.shiftingCameraModel == pict.cameraModel:
            self.adjustAllSameModel(pict, newTimestamp)
        else:
            # Apply only to this pict
            pict.timestamp = newTimestamp
            # Don't bother finding the new place, just sort again
            previews.sort(key=lambda prvw: prvw.mediafile.timestamp)
        self.layoutChanged.emit()
        # Return the index of the initial preview item
        return previews.index(refPreview)

    def setShiftingCameraModel(self, cameraModel: str) -> None:
        self.shiftingCameraModel = cameraModel
        self.layoutChanged.emit()

    def deletePict(self, idx: int):
        """ Mark the mediafile as toBeDeleted, and remove it from the GUI model """
        data = self.previews[idx]
        pict = data.mediafile
        pict.toBeDeleted = True
        pict.deletedByUser = True
        self.previews.remove(data)
        self.layoutChanged.emit()
