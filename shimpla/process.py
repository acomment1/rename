import os
from typing import List, TextIO

from send2trash import send2trash

from .core import cfg, MediaFile, JPG, MP4
from .duplicates import assign_unique_names, find_duplicates, computePerceptiveHash
from .metadata import fixDroneMetadata, updateJpegMetadata, retrieveMetadata, escapeWordForShell, updateMp4Metadata
from .timestamps import computeTimestamps


def loadFromDisk(picts: [MediaFile], do_find_dups: bool) -> None:
    buildFilesList(picts)
    retrieveMetadata(picts)
    computeTimestamps(picts)
    if do_find_dups:
        print(f"Finding duplicates in {len(picts)} pictures")
        computePerceptiveHash(picts)
        find_duplicates(picts)
        print("Finished finding duplicates")
    # Sort by timestamp and file name, only this first time
    picts.sort(key=lambda x: (x.timestamp, x.origName))

def buildFilesList(picts: [MediaFile]) -> None:
    for fileName in os.listdir('.'):
        # show progress when loading many files
        base, sep, extension = fileName.rpartition('.')
        if not base:
            # Irrelevant name format returns base='', sep='', extension=fileName
            continue
        print(f"{fileName}\x1b[0K", end='\r', flush=True)
        ext = None
        if extension == 'JPG' or extension == 'jpeg' or extension == 'jpg':
            ext = JPG
        if extension == 'MP4' or extension == 'mp4':
            ext = MP4
        # TODO: support of png and other video formats
        if ext:
            medFile = MediaFile(fileName, base, ext, os.path.getsize(fileName))
            if base.startswith('.trashed'):
                medFile.toBeDeleted = True    # cleanup Pixel trash can
            picts.append(medFile)
            if cfg.verbosity > 2:
                print(f"Included {medFile.origName}")

def list_changes(picts: [MediaFile]) -> str:
    result: str = "Timestamp              New Name                Old name"
    for pict in picts:
        description = f"{pict.timestamp.strftime('%Y-%m-%d %H:%M:%S')}   "
        if pict.toBeDeleted:
            description = description + "     deleted"
            if pict.origName.startswith('.trashed'):
                description = description + " is trash"
            elif pict.deletedByUser:
                description = description + " by user"
            else:
                description = description + "        "
        else:
            description = description + f" {pict.buildNewName()}"

        if pict.needRename() or pict.toBeDeleted:
            description = description + f"  <- {pict.origName}"
        if pict.computedTimestamp:
            description = description + " ***"
        elif pict.origTimestamp != pict.timestamp:
            description = description + f" adjusted by {pict.timestamp - pict.origTimestamp}"
        if pict.dupOf:
            description = description + f" duplicate d={pict.hammingDistance} of {pict.dupOf}"
            if not pict.toBeDeleted and not pict.computedTimestamp:
                description = description + " not deleted because it has Exif timestamp"
        result = result + '\n' + description
    return result

def computeChanges(picts: [MediaFile]) -> str:
    picts.sort(key=lambda x: (x.timestamp))
    assign_unique_names(picts)
    return list_changes(picts)

def generate_script(picts: [MediaFile]) -> None:
    with open('shimpla-script.sh', 'w') as script:
        fixDroneMetadata(picts, script)
        apply_changes(picts, script)

def apply_changes(picts: [MediaFile], script: TextIO = None) -> None:
    """ Apply the computed changes.
        Try to detect and no-op when the file was actually already OK as input
    """
    if script is not None:
        script.write('### File changes:\n')
    for pict in picts:
        if not os.path.isfile(pict.origName):
            # File was deleted by an external viewer, or by us in a previous run of this function
            continue
        if pict.toBeDeleted:
            if script is None:
                send2trash(pict.origName)
            else:
                script.write(f"gio trash {escapeWordForShell(pict.origName)}\n\n")  # Double line-feed because short loop exit
            continue

        modified: bool = False
        newPictName = pict.newBaseName+'.'+pict.extension
        needRename = newPictName != pict.origName

        if pict.extension == JPG:
            if updateJpegMetadata(pict, script):
                modified = True
        else: # mp4
            if updateMp4Metadata(pict, script):
                modified = True

        # set file timestamp (even if the file was actually not changed in any other way)
        desiredFileTimestamp = int(pict.timestamp.timestamp())
        actualFileTimestamp = int(os.path.getmtime(pict.origName))
        if desiredFileTimestamp != actualFileTimestamp:
            modified = True
            if script is None:
                os.utime(pict.origName, (desiredFileTimestamp, desiredFileTimestamp))
            else:
                script.write(f"touch -d '{pict.timestamp.strftime('%Y-%m-%d %H:%M:%S')}' {escapeWordForShell(pict.origName)}\n")

        if needRename:    # rename at the very end, because original name is helpful for disgs
            modified = True
            if script is None:
                os.rename(pict.origName, newPictName)
                pict.origName = newPictName         # Reset the MediaFile field
                pict.origBase = pict.newBaseName    # Reset the MediaFile field
            else:
                script.write(f"mv {escapeWordForShell(pict.origName)} {newPictName}\n")
        if script is not None and modified:
            script.write('\n')  # One blank line to separate from the next MediaFile
    # remove the deleted items from picts
    if script is None:
        picts[:] = [x for x in picts if not x.toBeDeleted]

def applyChanges(picts: [MediaFile]) -> None:
    picts.sort(key=lambda x: (x.timestamp))
    assign_unique_names(picts)
    fixDroneMetadata(picts)
    apply_changes(picts)
    retrieveMetadata(picts)  # exiv2 does not support that the file name changes

def generateScript(picts: [MediaFile]) -> None:
    picts.sort(key=lambda x: (x.timestamp))
    assign_unique_names(picts)
    generate_script(picts)