import argparse
import imagehash
from dataclasses import dataclass
from datetime import datetime

from exiftool import ExifToolHelper

JPG: str = 'jpg'
MP4: str = 'mp4'
WHATSAPP: str = "WhatsApp"
SOCIALNETWORK: str = "SocialNetwork"
@dataclass
class Configuration:
    verbosity: int = 0
    preview: bool = False
    keep_duplicates: bool = False
    graphics: bool = False
    generate_script: bool = False

cfg = Configuration()
exifToolHelper = ExifToolHelper()

def init():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbosity", nargs='?', const=1, type=int)
    parser.add_argument("-n", "--preview", action="store_true")
    parser.add_argument("-k", "--keep_duplicates", action="store_true")
    parser.add_argument("-g", "--graphics", action="store_true")
    parser.add_argument("-s", "--script", action="store_true")
    args = parser.parse_args()
    if args.verbosity == None:
        args.verbosity = 0
    if args.preview and args.verbosity == 0:
        args.verbosity = 1
    global cfg
    cfg.verbosity = args.verbosity
    cfg.preview = args.preview
    cfg.keep_duplicates = args.keep_duplicates
    cfg.graphics = args.graphics
    cfg.generate_script = args.script
    print(cfg)

def cleanup():
    global exifToolHelper
    exifToolHelper.terminate()

class MediaFile:
    def __init__(self, origName: str, origBase: str, extension:str, length:int):
        self.origName: str = origName
        self.origBase: str = origBase
        self.extension: str = extension
        self.length: int = length         # Approximative, may be changed by exiv2
        self.origTimestamp = None         # read from Exif, if any
        self.timestamp: datetime = None   # Possibly adjusted with the GUI
        self.newBaseName: str = None      # computed by fix_name_collisions(), reset when applied
        self.computedTimestamp: bool = False  # timestamp is not from Exif, or inital image is from SocialNetwork
        self.metadata: dict = None        # as retrieved by exiftool
        self.missingMetadata: bool = True
        self.metadataToBeUpdated: bool = False  # When the initial timestamp is wrong, for example
        self.cameraModel: str = None                # Camera model, possibly WhatsApp
        self.cameraMaker: str = None                # Canon, Google, or SocialNetwork
        self.fileHash: imagehash.ImageHash = None
        self.dupOf = None
        self.hammingDistance: int = None  # Only for summary display or diags
        self.toBeDeleted: bool = False
        self.deletedByUser: bool = False
        #self.thumbnail = None

    def buildNewName(self) -> str:
        newBaseName = self.newBaseName if self.newBaseName is not None else '?????????' # Throw exception instead ?
        return self.newBaseName+'.'+self.extension

    def needRename(self) -> bool:
        if self.newBaseName is None:
            return False
        return self.buildNewName() != self.origName

