from typing import Tuple

from PyQt5.QtCore import Qt, QRect, QSize, QModelIndex, QItemSelectionModel, QProcess
from PyQt5.QtGui import QImage, QPen, QPalette, QFont

from PyQt5.QtWidgets import QStyledItemDelegate, QTableView, QMainWindow, QApplication

from .core import *
from .model import PreviewData


class RenameTableView(QTableView):

    MIN_CELL_WIDTH = 100
    MAX_CELL_WIDTH = 500

    def __init__(self, window: QMainWindow):
        super().__init__()
        self.window = window
        self.horizontalHeader().hide()
        self.verticalHeader().hide()
        self.setGridStyle(Qt.NoPen)

        # The thumbnails are displayed in a table, numColumns is the number of columns
        self.numColumns = 6
        # Each thumbnail must fit into a box this large, including padding
        self.cell_width = 300
        self.cell_height = 200  # form factor: cell_width * 2 // 3
        # This is all we care about in this class. The details of fitting a thumbnail in the cell are
        # handled by the PreviewDelegate

        self.actionConfig = None
        self.process = None

    def setContextMenuBuilder(self, actionsCfg):
        self.actionConfig = actionsCfg

    def contextMenuEvent(self, event):
        self.actionConfig.configureContextMenu(event)

    def selectByCameraModel(self, cameraModel: str) -> None:
        for idx, item in enumerate(self.model().previews):
            if item.mediafile.cameraModel == cameraModel:
                self.selectionModel().select(
                    self.getModelIndex(idx),
                    QItemSelectionModel.Select
                )

    def mouseDoubleClickEvent(self, event):
        """ Call an external viewer """
        # TODO: call vlc on mp4
        modIdx: QModelIndex = self.currentIndex()
        if not modIdx.isValid():
            return
        pict: MediaFile = modIdx.data().mediafile
        if pict.extension == JPG:
            viewer = "gwenview"
        elif pict.extension == MP4:
            viewer = "vlc"
        else:
            return
        self.process = QProcess()
        self.process.start(viewer, [pict.origName])
        if cfg.verbosity > 1:
            print(f"{viewer} started")

    def getScalarIndex(self, modelIndex) -> int:
        return modelIndex.row() * self.numColumns + modelIndex.column()  # Possibly more than previews[] count

    def getModelIndex(self, scalarIndex: int) -> QModelIndex:
        return self.model().index(scalarIndex // self.numColumns, scalarIndex % self.numColumns)

    def numColumnsUpdated(self) -> int | None:
        # return the current scalar index, or None if the number of columns did not change
        newColCount = self.width() // self.cell_width
        if newColCount == 0:
            newColCount = 1
        if newColCount != self.numColumns:
            # compute the selection scalar index before numColumns changes
            scalarIndexToSelect: int = self.getScalarIndex(self.currentIndex())
            self.numColumns = newColCount
            return scalarIndexToSelect
        else:
            return None

    def onWindowResized(self):
        scalarIndexToSelect = self.numColumnsUpdated()
        if scalarIndexToSelect is not None:
            self.redraw(scalarIndexToSelect)

    def cellWidthChanged(self):
        self.cell_height = self.cell_width * 2 // 3
        scalarIndexToSelect = self.numColumnsUpdated()
        # redraw, even if the number of columns did not change
        self.redraw(scalarIndexToSelect)

    def redraw(self, scalarIndexToSelect: int) -> None:
        if scalarIndexToSelect is not None:
            self.clearSelection()
        self.model().layoutChanged.emit()
        self.resizeRowsToContents()
        self.resizeColumnsToContents()
        if scalarIndexToSelect is not None:
            modelIndex = self.getModelIndex(scalarIndexToSelect)
            self.setCurrentIndex(modelIndex)

    def wheelEvent(self, event):
        modifiers = QApplication.keyboardModifiers()
        if modifiers == Qt.ControlModifier:
            x = event.angleDelta().y() / 120
            if x > 0:
                if self.cell_width < self.MAX_CELL_WIDTH:
                    self.cell_width += 5
                    self.cellWidthChanged()
                # print(f"zoom in, cell_width={self.cell_width}")
            elif x < 0:
                if self.cell_width > self.MIN_CELL_WIDTH:
                    self.cell_width -= 5
                    self.cellWidthChanged()
                # print(f"zoom out, cell_width={self.cell_width}")
        else:
            super().wheelEvent(event)

    # Controller (integrated to the view) ########################################
    def getReferencedPict(self, acceptShiftingCameraModel: bool) -> Tuple[MediaFile, int] | Tuple[None, None]:
        """ Check that the current index is valid, and points to a computedTimestamp image, or a  shiftingCameraModel"""
        modIdx: QModelIndex = self.currentIndex()
        if not modIdx.isValid():
            return None, None
        idx: int = self.getScalarIndex(modIdx)
        preview = modIdx.data()
        if preview is None:
            return None, None
        pict: MediaFile = preview.mediafile
        if pict.computedTimestamp:
            return pict, idx
        if pict.cameraModel == self.model().shiftingCameraModel and acceptShiftingCameraModel:
            return pict, idx
        return None, None

    def movePict(self, moveAmount: int) -> None:
        if cfg.verbosity > 2:
            print(f"Move by {moveAmount}")
        pict, idx = self.getReferencedPict(acceptShiftingCameraModel=True)
        if pict is None:
            return
        targetIdx: int = idx + moveAmount
        if targetIdx < 0 or targetIdx >= len(self.model().previews):
            return  # target is out of bounds
        # Call the Model to do the job
        targetScalarIndex: int = self.model().movePict(idx, moveAmount)
        # Select target, so that the selection follows the move
        self.setCurrentIndex(self.getModelIndex(targetScalarIndex))

    def moveCloser(self, direction: int):
        pict, idx = self.getReferencedPict(acceptShiftingCameraModel=True)
        if pict is None:
            return
        self.model().moveCloser(idx, direction)

    def moveByHour(self, moveAmount: int) -> None:
        if cfg.verbosity > 2:
            print(f"moveByHour({moveAmount})")
        pict, idx = self.getReferencedPict(acceptShiftingCameraModel=True)
        if pict is None:
            return
        self.model().moveByHour(idx, moveAmount)

    def deletePict(self):
        """ Mark the mediafile as toBeDeleted, and remove it from the GUI model """
        pict, idx = self.getReferencedPict(acceptShiftingCameraModel=False)
        if pict is None:
            return
        self.model().deletePict(idx)


class PreviewDelegate(QStyledItemDelegate):

    fontHeight = 10
    timestampFont = QFont()
    timestampFont.setPointSize(fontHeight)

    def __init__(self, view: RenameTableView):
        super().__init__()
        self.tableView = view

    def paint(self, painter, option, index):
        hCellPadding: int = 8  # right and left
        vCellPadding: int = 8  # top only
        vTextPaddingTop: int = 3  # Between image and text
        vTextPaddingBottom: int = 2  # Between text and next cell
        vTextAreaSize: int = vTextPaddingTop + self.fontHeight + vTextPaddingBottom

        cell_padding = min(self.tableView.cell_height // 10, 8)  # all sides, 20 initially

        # data is our preview object
        model = index.model()
        data = model.data(index, Qt.DisplayRole)
        if data is None:
            return  # Probably a cell in the last, incomplete row
        pData: PreviewData = data.previewdata
        # option.rect holds the area we are painting on the widget (our table cell)
        # Compute the room available for the thumbnail, taking padding and date text in account
        thumbAllocatedWidth = option.rect.width() - hCellPadding * 2
        thumbAllocatedHeight = option.rect.height() - vCellPadding - vTextAreaSize
        if pData.thumbnail is None or pData.lastThumbWidth != self.tableView.cell_width:
            # Should we check if height changed ?
            # The allocated cell dimension has changed, (re)-scale the thumbnail, to fit
            # scale our pixmap to fit
            if cfg.verbosity > 3:
                print(f"Scaling {data.mediafile.origName} to {thumbAllocatedWidth}x{thumbAllocatedHeight}")
            pData.thumbnail = pData.largeThumb.scaled(
                thumbAllocatedWidth,
                thumbAllocatedHeight,
                aspectRatioMode=Qt.KeepAspectRatio,
            )
            pData.lastThumbWidth = self.tableView.cell_width    # Track cell_width, to avoid re-scaling every time
            pData.lastCellHeight = self.tableView.cell_height
        thumb: QImage = pData.thumbnail
        # Position in the middle of the area.
        # Due to variable from factor, the actual coordinates of the thumb must be adjusted to keep it centered
        # In python 3, classic integer division uses the // operator
        thumbX = hCellPadding + (thumbAllocatedWidth - thumb.width()) // 2   # integer division uses the // operator
        thumbY = cell_padding + (thumbAllocatedHeight - thumb.height()) // 2

        painter.drawImage(option.rect.x() + thumbX, option.rect.y() + thumbY, thumb)
        # textVSpacer = 4  # Two pixels between image and text
        # textHeight = cell_padding//2 + 6  # Text box may extend lower than bottom of cell (use next cell's margin)
        textRect = QRect(
                        option.rect.x() + hCellPadding,
                        option.rect.y() + option.rect.height() - vTextPaddingBottom - self.fontHeight,  # bottom up
                        thumbAllocatedWidth,
                        self.fontHeight)
        if data.mediafile.cameraModel == model.shiftingCameraModel:
            # Draw a thick rectangle around the cell, using the Highlight Color, to indicate its sort-of-selection
            painter.setPen(QPen(option.palette.color(QPalette.Highlight).lighter(), cell_padding, Qt.SolidLine))
            painter.drawRect(option.rect.x() + cell_padding//2,
                             option.rect.y() + cell_padding//2,
                             option.rect.width() - cell_padding,
                             option.rect.height() - cell_padding)
            painter.setPen(Qt.red)  # Dates that you can change are displayed in red
        elif data.mediafile.computedTimestamp:
            painter.setPen(Qt.red)  # Dates that you can change are displayed in red
        else:
            painter.setPen(Qt.black)
        # TODO flag also the duplicates, including those not automatically removed because having Exif timestamp
        painter.setFont(self.timestampFont)
        painter.drawText(textRect, Qt.AlignCenter, data.mediafile.timestamp.strftime("%Y-%m-%d %H:%M:%S"))

    def sizeHint(self, option, index):
        # All items the same size.
        return QSize(self.tableView.cell_width, self.tableView.cell_height)
