from PyQt5.QtGui import QResizeEvent
from PyQt5.QtWidgets import QApplication, QMainWindow, QMenu, QToolBar

from .actions import ActionsConfigurator
from .core import MediaFile
from .model import PreviewModel
from .view import RenameTableView, PreviewDelegate


class MainWindow(QMainWindow):
    def __init__(self, picts: [MediaFile], app: QApplication):
        super().__init__()

        self.picts = picts
        self.app = app

        self.view = RenameTableView(self)

        delegate = PreviewDelegate(self.view)
        self.view.setItemDelegate(delegate)
        self.model = PreviewModel(self.view)
        self.view.setModel(self.model)

        self.setCentralWidget(self.view)

        self.model.loadPicts(self.picts, self.style())

        self.view.resizeRowsToContents()
        self.view.resizeColumnsToContents()

        menuBar = self.menuBar()
        fileMenu = QMenu("&File", self)
        menuBar.addMenu(fileMenu)

        actionsToolBar: QToolBar = self.addToolBar('actions')

        self.actionsCfg = ActionsConfigurator(app, self, self.view, picts)
        self.actionsCfg.configureViewActions()
        self.actionsCfg.configureFileMenuActions(fileMenu)
        self.actionsCfg.configureToolBarActions(actionsToolBar, menuBar)
        self.view.setContextMenuBuilder(self.actionsCfg)

    def resizeEvent(self, event: QResizeEvent) -> None:
        self.view.onWindowResized()


def run_gui(picts: [MediaFile]) -> int:
    app = QApplication(["shimpla"])
    window = MainWindow(picts, app)
    window.show()
    return app.exec_()

# Various useful sites:
# https://www.pythonguis.com/faq/file-image-browser-app-with-thumbnails/
# https://www.pythonguis.com/tutorials/pyqt-signals-slots-events/
# https://www.pythonguis.com/faq/built-in-qicons-pyqt/
# https://doc.qt.io/qt-6/model-view-programming.html
