from datetime import timedelta
from PIL import Image

from .core import cfg, MediaFile
from .patterns import cannonicalNamePattern, pixelDerivedPattern, derivativeSuffixPattern, pixelExportedPattern

from .core import *

def buildPerceptiveHash(pict:MediaFile) -> None:
    image = Image.open(pict.origName)
    # hash = imagehash.phash(image) # perceptive hash is often better, but sometimes erratic
    # hash = imagehash.dhash(image)
    hash = imagehash.whash(image)  # whash is not fast, but best against jpg compression ans scaling (our usecase)
    pict.fileHash = hash

def computePerceptiveHash(picts: [MediaFile]) -> None:
    for pict in picts:
        if pict.extension == JPG:
            buildPerceptiveHash(pict)

def find_duplicates(picts: [MediaFile]) -> None:
    """
    Plain comparison with all other hashes.
    Tests with a sample of 291 files have shown that this brute-force compare adds negligible
    duration (0.2 seconds) to the hash-computation duration (12 seconds).
    It brings also an easy possibilty to detect low-but-non-zéro hamming distances, without a binary tree.
    Needs computedTimestamp to be set
    """
    for img in picts:
        # Looking for duplicates of img (img is the original)
        if img.fileHash == None:
            continue
        if img.dupOf:
            # Do not search duplicates of duplicates
            continue
        for candidate in picts:
            # candidate could be duplicate of img
            if candidate.fileHash == None:
                continue
            if img.origName == candidate.origName:
                continue
            if candidate.dupOf:
                continue  # already a dup of somebody, no processing needed
            if candidate.length > img.length:
                continue   # the duplicate must be smaller or equal to than the original (possibly same picture but less metadata)
            hamming_distance = img.fileHash - candidate.fileHash
            if cfg.verbosity > 2 and hamming_distance < 4:
                print(f"Hamming distance {hamming_distance} between {img.origName} and {candidate.origName}")
            if hamming_distance < 2: # Careful tuning here, depends on type of hamming distance
            #    print(f"Hamming distance: {d}  for {img.fileName} and {candidate.fileName}")
                candidate.dupOf = img.origName
                candidate.hammingDistance = hamming_distance
                # delete the candidate only if it comes from WhatsApp or Signal
                if candidate.computedTimestamp:
                    candidate.toBeDeleted = True  # a WhatsApp image that we are currently importing
                elif candidate.cameraModel == SOCIALNETWORK:
                    candidate.toBeDeleted = True  # an image from WhatsApp or Signal
                else:
                    if cfg.verbosity > 1:
                        print(f"Not removing {candidate.origName} duplicate of {img.origName} d={hamming_distance} because it has Exif timestamp")

def compute_newBaseName(pict: MediaFile) -> str:
    newBaseName = pict.timestamp.strftime('%Y%m%d_%H%M%S')
    m = pixelExportedPattern.search(pict.origBase)
    if m:  # Exported files are named just like any other file
        return newBaseName
    # Catch derivative work on Pixel, named like PXL_20230831_091250655~2.jpg, and preserve suffix
    m = pixelDerivedPattern.search(pict.origBase)
    if m:
        if m.group('suffix') == '.LS':  # ex: PXL_20240211_131834411.LS.mp4
            return newBaseName  # Just discard the .LP, that means "Long Shot", (long press on button) but it is a plain 1024x768 hevc video
        if m.group('suffix') == '.NIGHT':  # ex: PXL_20240213_150350425.NIGHT.jpg
            return newBaseName  # Just discard the .NIGHT, that means a special camera settings
        if m.group('suffix') == '.MP':  # ex: PXL_20240210_204736843.MP.jpg
            return newBaseName # a MotionPhoto jpeg, includes short video in JPEG trailer. The trailer is removed in updateJpegMetadata
        # Sanitize the derivative suffix, ex: PXL_20231227_131733772~(1).jpg  -> 20231227_131733772~1.jpg
        n = derivativeSuffixPattern.search(m.group('suffix'))
        if n:
            newBaseName = newBaseName + '-' + n.group('derivnum')
        else:
            newBaseName = newBaseName + m.group('suffix')
    # If current name is identical, except for a suffix like 1 or -1, keep current name
    # Keep meaningful derivation-work suffixes (like -1), and also avoids flip-flop when the command is run again
    if pict.origBase.startswith(newBaseName):
        return pict.origBase
    # default case
    return newBaseName

def fix_name_collisions(pict: MediaFile, idx: int, picts: [MediaFile]) -> None:
    # Compute pict.newBaseName
    if cfg.verbosity > 1:
        print(f"Checking name collisions for [{idx}] {pict.origBase}")
    tries = 0
    while True:
        tries = tries + 1
        # Compute a new basename, based on timestamp
        # do not recompute for non-computedTimestamp, since it would defeat the collision-rename scheme
        if pict.computedTimestamp or tries == 1:
            pict.newBaseName = compute_newBaseName(pict)
        # Check for duplicate name (already assigned previously)
        if idx == 0:
            break  # first pict in list cannot have a duplicate
        collision = False
        for ri, candidate in enumerate(picts):
            if candidate == pict:
                # Do not check with ourselves
                continue
            if cfg.verbosity > 3:
                print(f"   ri={ri} checking {candidate.origBase}")
            if candidate.newBaseName == pict.newBaseName:
                # found a pict already named, that has the same name as this one
                # picts that have not been processed yet have a null newBaseName, and get ignored
                collision = True
                if pict.computedTimestamp:
                    # If the timestamp was parsed or interpolated, just bump the timestamp by one second
                    if cfg.verbosity > 1:
                        print("Collision, add 1s and try again")
                    pict.timestamp = pict.timestamp + timedelta(seconds=1)
                else:
                    if tries == 1:
                        # Save the basename to generate xxx1, xxx2, xxx3 and not xxx1, xxx12, xxx123
                        refNewBaseName = pict.newBaseName
                    pict.newBaseName = refNewBaseName + str(tries)
                    if cfg.verbosity > 1:
                        print(f"Collision, append {tries} and try again with {pict.newBaseName}")
                # We found a duplicate, no need to continue checking previous items
                break
        if not collision:
            # We got a unique name, exit the while loop: we are done with this pict
            break
        if tries >= 100:
            print(f"Already {tries} attempts to buid unique name, something wrong with {pict.origName}, exiting")
            exit(10)
        # at this point, we have found a collision and applied a fix, let us try again
    if cfg.verbosity > 1:
        print('')

def assign_unique_names(picts: [MediaFile]) -> None:
    # Assign a new name based on the timestamp
    # To avoid extra renaming when adding new files to an already renamed repertory,
    # we process cannonical names first, and then the other files.
    # The added files will be the ones that experience collision and name fixes, while the
    # cannonical files remain unchanged
    for idx, pict in enumerate(picts):
        if pict.toBeDeleted:
            continue
        m = cannonicalNamePattern.search(pict.origBase)
        if m:
            fix_name_collisions(pict, idx, picts)

    for idx, pict in enumerate(picts):
        if pict.newBaseName == None:
            fix_name_collisions(pict, idx, picts)
