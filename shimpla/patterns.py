import re

# TODO support pf vlcsnap-2023-08-13-10h04m55s943.jpg
# Format of files that have already been renamed by us (or are named like this naturally, ex Samsung)
cannonicalNamePattern = re.compile(
    '^(?P<year>20\d{2})(?P<month>\d{2})(?P<day>\d{2})_(?P<hour>\d{2})(?P<minute>\d{2})(?P<sec>\d{2})$'
)
# File nmaes that embed a canonical timestamp, anywhere in the name.
# Ex: .trashed-1701174686-PXL_20231029_123104775.mp4
datedNamePattern = re.compile(
    '(?P<year>20\d{2})(?P<month>\d{2})(?P<day>\d{2})_(?P<hour>\d{2})(?P<minute>\d{2})(?P<sec>\d{2})'
)
# Pixel pictures names after on-device edition, eg: PXL_20230831_091250655~2.jpg
pixelDerivedPattern = re.compile(
    '^PXL_\d{8}_\d{9}(?P<suffix>.*)'
)
# Pixel format for jpg extracted from a video: PXL_20240104_145006462_exported_6054.jpg
# or from a MotionPicture: PXL_20240211_135113963_exported_1000_1707659507316.jpg
# The pattern is matching the base name, no extension
pixelExportedPattern = re.compile(
    '^PXL_(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})_(?P<hour>\d{2})(?P<minute>\d{2})(?P<sec>\d{2})\d{3}_exported_(\d|_)*')
# Linux naming is like: WhatsApp Image 2023-08-16 at 15.23.32.jpeg
linuxWhatsAppPattern = re.compile(
    '^WhatsApp (Image|Video) (?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}) at (?P<hour>\d{2})\.(?P<minute>\d{2})\.(?P<sec>\d{2})')
# Android client naming is like: IMG-20230827-WA0001.jpg
androidWhatsAppPattern = re.compile(
    '^(IMG|VID)-(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})-WA\d{4}')

# ffprobe:  creation_time   : 2023-09-03T11:37:29.000000Z
creationDatePattern = re.compile(
    '\s*creation_time\s*:\s*(?P<isotime>\S+)')
# ffprobe, for Iphone videos
iphoneCreationDatePattern = re.compile(
    '\s*com.apple.quicktime.creationdate\s*:\s*(?P<isotime>\S+)')

# Derivative-suffix sanitization and normalization
derivativeSuffixPattern = re.compile(
    '[-~( \s]*(?P<derivnum>\d+)[-~) \s]*')
