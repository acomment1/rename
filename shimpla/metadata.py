from typing import TextIO
import subprocess

from exiftool.exceptions import ExifToolExecuteError

from .core import cfg, exifToolHelper, MediaFile, JPG, SOCIALNETWORK, MP4, WHATSAPP
from .timestamps import exifTs2PythonTs, pythonTs2exifTs, pythonTs2mp4Ts, mp4Ts2PythonTs


def escapeWordForShell(input: str) -> str:
    if ' ' in input:
        result = input.replace("'", r"\'")
        return f"'{result}'"
    else:
        return input

def run_command(cmd_args: [], script: TextIO = None):
    charsWritten = 0
    if script is not None:
        for word in cmd_args:
            charsWritten += script.write(escapeWordForShell(word))
            charsWritten += script.write(' ')
        charsWritten += script.write('\n')
    else:
        if cfg.verbosity > 1:
            print(f"Running {' '.join(cmd_args)}")
        command = subprocess.run(cmd_args, capture_output=True, text=True)
        if command.returncode:
            print(f"{' '.join(cmd_args)} FAILED, error: {command.stderr}")
    return charsWritten


def retrieveMetadata(picts: [MediaFile]) -> None:
    for pict in picts:
        if pict.extension == JPG or pict.extension == MP4:
            try:
                metadata = exifToolHelper.get_metadata(pict.origName)[0]
            except ExifToolExecuteError:
                print(f"Exiftool failed on file {pict.origName} - Please fix or remove this file\n")
                exit(4)
            pict.metadata = metadata
        if pict.extension == JPG:
            exifTs = metadata.get('EXIF:DateTimeOriginal')
            if exifTs is None:
                exifTs = metadata.get('EXIF:ModifyDate')
            if exifTs is None:
                exifTs = metadata.get('EXIF:CreateDate')
            if exifTs is not None:
                pict.timestamp = exifTs2PythonTs(exifTs)
                pict.missingMetadata = False
            pict.cameraModel = metadata.get('EXIF:Model')
            pict.cameraMaker = metadata.get('EXIF:Make')
            if pict.cameraMaker == SOCIALNETWORK:
                pict.computedTimestamp = True    # Indicate that we can fiddle with the timestamp
        elif pict.extension == MP4:
            maker = metadata.get('QuickTime:Make')
            if maker is not None:
                pict.cameraMaker = maker
                pict.missingMetadata = False
                if pict.cameraMaker == SOCIALNETWORK:
                    pict.computedTimestamp = True    # Indicate that we can fiddle with the timestamp
            model = metadata.get('QuickTime:Model')
            if model is not None:
                pict.cameraModel = model
                pict.missingMetadata = False
            extlTs: str = metadata['QuickTime:CreateDate']
            if extlTs != '0000:00:00 00:00:00':
                pict.timestamp = mp4Ts2PythonTs(extlTs)
                pict.missingMetadata = False

def updateJpegMetadata(pict: MediaFile, script: TextIO = None) -> bool:
    forReal: bool = script is None
    needTagsUpdate: bool = False
    modified: bool = False
    exifTs = pythonTs2exifTs(pict.timestamp)
    # We will need the following 3 tags in any case
    TagsToUpdate = {
        'EXIF:DateTimeOriginal': exifTs,
        'EXIF:CreateDate': exifTs,
        'EXIF:ModifyDate': exifTs,
    }
    if pict.missingMetadata:
        needTagsUpdate = True
        if forReal:
            # Creating the EXIF header, time to set our "virtual" Make and Model
            if pict.cameraMaker is not None:
                TagsToUpdate['EXIF:Make'] = pict.cameraMaker
            if pict.cameraModel is not None:
                TagsToUpdate['EXIF:Model'] = pict.cameraModel
        else:
            run_command(["jhead", "-mkexif", pict.origName], script)
            if pict.cameraModel == WHATSAPP:
                run_command(["exiv2",
                                        f"-Mset Exif.Image.Make {SOCIALNETWORK}",
                                        f"-Mset Exif.Image.Model {WHATSAPP}",
                                        pict.origName], script)
    else:
        if pict.origName.startswith('PXL_') and pict.metadata.get('XMP:MotionPhoto') == 1:
            modified = True
            # Incantation to remove the extra MotionPhoto data, in the JPEG Trailer
            # Remove also XMP tags indicating presence of a MotionPhoto in the jpeg file
            # HACK: assume that XMP-Container and XMP-Camera are only aboout MotionPhoto
            if forReal:
                exifToolHelper.execute(
                    "-trailer:all=",
                    "-XMP-Container:all=",
                    "-XMP-GCamera:all=",
                    "-overwrite_original",
                    pict.origName)
            else:
                run_command(["exiftool",
                             "-trailer:all=",
                             "-XMP-Container:all=",
                             "-XMP-GCamera:all=",
                             "-overwrite_original",
                             pict.origName],
                            script)
        if pict.metadataToBeUpdated or ( pict.origTimestamp is not None and pict.timestamp != pict.origTimestamp ):
            needTagsUpdate = True
            # Update all tiemstamps
            if not forReal:
                # -dsft to update the Exif timestamps
                run_command(["jhead", "-dsft", "-autorot", "-dt", pict.origName], script)
    if needTagsUpdate and forReal:
        exifToolHelper.set_tags(
            pict.origName,
            tags=TagsToUpdate,
            params=["-overwrite_original"]
        )
    if needTagsUpdate or modified:
        # Delete the thumbnail whenever we modify the file
        # With exiftool, this means deleting all items in the exif group IFD1
        # exiftool -ifd1:all= -FILE
        exifToolHelper.execute("-ifd1:all=", "-overwrite_original", pict.origName)
        pict.origTimestamp = pict.timestamp  # Reset the MediaFile field
        pict.missingMetadata = False         # Reset the MediaFile field

    return needTagsUpdate or modified

def updateMp4Metadata(pict: MediaFile, script: TextIO = None) -> bool:
    forReal: bool = script is None
    needUpdate: bool = False
    utcTs = pythonTs2mp4Ts(pict.timestamp)
    exTags = {'QuickTime:CreateDate': utcTs}
    if pict.missingMetadata:
        needUpdate = True
        if forReal:
            if pict.cameraMaker is not None:
                exTags['QuickTime:Make'] = pict.cameraMaker
            if pict.cameraModel is not None:
                exTags['QuickTime:Model'] = pict.cameraModel
        else:
            run_command(["exiftool", f"-CreateDate<{utcTs}", "-overwrite_original", pict.origName], script)
    elif pict.origTimestamp is not None and pict.timestamp != pict.origTimestamp:
        needUpdate = True
        if not forReal:
            run_command(["exiftool", f"-CreateDate<{utcTs}", "-overwrite_original", pict.origName], script)
    if needUpdate:
        exifToolHelper.set_tags(
            pict.origName,
            tags=exTags,
            params=["-overwrite_original"]
        )
    return needUpdate

def fixDroneMetadata(picts: [MediaFile], script: TextIO = None) -> None:
    """ Remove unwanted metadata in DGI files """
    forReal: bool = script is None
    if script is not None:
        script.write('### Sanitizing DJI files:\n')
    for pict in picts:
        if pict.toBeDeleted:
            continue
        if not pict.origName.startswith("DJI_"):
            continue
        if pict.extension == JPG:
            # Exif.Image.ImageDescription                  Ascii      27  DCIM\100MEDIA\DJI_0001.JPG
            # Exif.Image.XPComment                         Byte      256  Type=N, Mode=P, DE=None
            # Exif.Image.XPKeywords                        Byte      256  v00.00.0637;0.0.1;v1.0.0
            # XPKeywords is sourced by digikam and creates 3 additional tags. We do not want that.
            if forReal:
                # setTags not helpful for deleting a tag
                exifToolHelper.execute("-EXIF:ImageDescription=",
                                       "-EXIF:XPComment=",
                                       "-EXIF:XPKeywords=",
                                       "-overwrite_original",
                                       pict.origName)
            else:
                run_command(["exiv2",
                            '--Modify', 'del Exif.Image.ImageDescription',
                            '--Modify', 'del Exif.Image.XPKeywords',
                            '--Modify', 'del Exif.Image.XPComment',
                            pict.origName], script)
        if pict.extension == MP4:
            # Remove Comment metadata in mp4 files, because they are read but not written by digikam,
            # so the edition gets lost when reloading metadata
            # Ex: comment : DE=None,SN=1SFLK320AL0D34, Type=Normal, HQ=Normal, Mode=P
            if forReal:
                exifToolHelper.execute("-QuickTime:Comment=", "-overwrite_original", pict.origName)
            else:
                run_command(["exiftool",
                            '-overwrite_original',
                            '-Comment=',
                            pict.origName], script)

