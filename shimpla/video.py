from importlib import resources

from PyQt5.QtCore import QSize
from PyQt5.QtGui import QImage, QPainter
from PyQt5.QtWidgets import QStyle

import cv2  # opencv-python-headless , since opencv-python causes a load error due to conflicting Qt libs

class Thumbnailer():
    def __init__(self, style:QStyle):
        playIcon = style.standardIcon(QStyle.SP_MediaPlay)
        self.playIcon = QImage(playIcon.pixmap(playIcon.actualSize(QSize(1024, 1024))))

        png_bytes = resources.read_binary("shimpla.images", "film-strip.png")
        self.strip:QImage = QImage()
        self.strip.loadFromData(png_bytes, 'png')

    def buildLargeVideoThumb(self, path:str) -> QImage:
        # from https://stackoverflow.com/questions/61454772/how-can-i-get-a-video-thumbnail-using-qt
        # max_size = (200,200) # set your thumbnail size
        video = cv2.VideoCapture(path)
        success, img = video.read()
        if success:
            # No need to resize here: resizing is performed in the delegate
            # img = cv2.resize(img,max_size,interpolation=cv2.INTER_AREA)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            rawThumb = QImage(img.data, img.shape[1], img.shape[0], QImage.Format.Format_RGB888)
        else:
            # Poor old version, displaying a Play symbol
            rawThumb = self.playIcon
        # Decorate with film-strip on the side
        thumb_width:int = rawThumb.width()
        thumb_height:int = rawThumb.height()
        result:QImage = QImage(thumb_width, thumb_height, QImage.Format_ARGB32)
        painter:QPainter = QPainter(result)
        painter.setCompositionMode(QPainter.CompositionMode.CompositionMode_Source)
        painter.drawImage(0, 0, rawThumb)
        # TODO: scale strip to 1/20 of actual thumb_width ? or not, displayed width indicates video resolution
        painter.drawImage(0, 0, self.strip)
        painter.drawImage(thumb_width-100, 0, self.strip)
        painter.end()
        return result
