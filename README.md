# shimpla



## What is it ?

A tool for easy renaming of pictures from multiple sources, with fuzzy or missing timestamps.  
SHift IMage to the right PLAce

## Main features

- Retrieve picture timestamps from Exif data, if any
- Parse file name to guess timestamp of WhatsApp shared images
- Rename all files to a single naming scheme: YYYYMMDD_HHMMSS
- Handle name collisions
- Show a GUI with thumbnails
- In the GUI, dragging an image adjusts its timestamp, keeping chronological order
- Group-shift of timestamps of a particular camera model (compensate for wrong camera clock)

## License
What ?

## TODO
Anticipate the 1-second shift performed by fix_name_collisions, so the new name is visible in the GUI
