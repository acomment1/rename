from shimpla.core import cfg, init, cleanup
from shimpla.metadata import fixDroneMetadata
from shimpla.process import list_changes, apply_changes, loadFromDisk, generate_script
from shimpla.duplicates import assign_unique_names
from shimpla.display import run_gui

def main():
    init()
    allFiles = []
    loadFromDisk(allFiles, not cfg.keep_duplicates)
    if cfg.graphics:
        print("Starting Graphical User Interface")
        run_gui(allFiles)
    else:
        assign_unique_names(allFiles)
        if cfg.verbosity > 0:
            print(list_changes(allFiles))
        if cfg.generate_script:
            generate_script(allFiles)
        if not cfg.preview:
            fixDroneMetadata(allFiles)
            # Renaming is last, because we need the original name for various fixes
            apply_changes(allFiles)
    cleanup()


if __name__ == "__main__":
    main()
